extends Area2D

signal hit


export var speed = 400  # How fast the player will move (pixels/sec).
var screen_size  # Size of the game window.

var target = Vector2(0, 0)

func _ready():
	screen_size = get_viewport_rect().size
	hide()


func _process(delta):
	var velocity = Vector2(0, 0)
	var using_touch = false
	
	if position.distance_to(target) > 10:
		# use touch controls
		using_touch = true
		velocity = target - position
	else:
		# use key controls
		if Input.is_action_pressed("ui_left"):
			velocity.x -= 1
		if Input.is_action_pressed("ui_right"):
			velocity.x += 1
		if Input.is_action_pressed("ui_up"):
			velocity.y -= 1
		if Input.is_action_pressed("ui_down"):
			velocity.y += 1
	
	if velocity.length() > 0:
		# we are moving, give us speed and animate
		velocity = velocity.normalized() * speed
		if velocity.x != 0:
			$AnimatedSprite.flip_h = (velocity.x < 0)
			$AnimatedSprite.animation = "walk"
		if velocity.y != 0:
			$AnimatedSprite.flip_v = (velocity.y > 0)
			# prefer the walk animation if applicable
			if velocity.x == 0:
				$AnimatedSprite.animation = "up"
		
		
		$AnimatedSprite.play()
	else:
		# no more animation
		$AnimatedSprite.stop()

	# update our position and clamp us to the screen
	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)
	
	if not using_touch:
		# clear any accumulated delta from touch 
		target = position


func _input(event):
	if event is InputEventScreenTouch and event.pressed:
		target = event.position


func _on_Player_body_entered(body):
	print_debug(body)
	hide()
	emit_signal("hit")
	$CollisionShape2D.set_deferred("disabled", true)


func start(pos):
	position = pos
	target = pos
	show()
	$CollisionShape2D.disabled = false
