extends Node


export (PackedScene) var Mob
var score


func _ready():
	randomize()


func _process(delta):
	pass


func new_game():
	score = 0
	$HUD.update_score(score)
	$Player.start($StartPosition.position)
	$StartTimer.start()
	$Music.play()
	

func game_over():
	$Music.stop()
	$DeathSound.play()
	$ScoreTimer.stop()
	$MobTimer.stop()
	get_tree().call_group("mobs", "queue_free")
	$HUD.show_game_over()


func _on_MobTimer_timeout():
	# Choose a random location on 2D path
	$MobPath/MobSpawnLocation.offset = randi()
	
	var mob = Mob.instance()
	add_child(mob)
	
	mob.position = $MobPath/MobSpawnLocation.position
	
	var direction = $MobPath/MobSpawnLocation.rotation + deg2rad(90)	# 90 degrees clockwise from the path, i.e. towards the middle
	direction += rand_range(deg2rad(-45), deg2rad(45))
	mob.rotation += direction
	
	mob.linear_velocity = Vector2(rand_range(mob.speed_min, mob.speed_max), 0)
	mob.linear_velocity = mob.linear_velocity.rotated(direction)


func _on_ScoreTimer_timeout():
	score += 1
	$HUD.update_score(score * 17)


func _on_StartTimer_timeout():
	$MobTimer.start()
	$ScoreTimer.start()

