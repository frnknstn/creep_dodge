extends RigidBody2D


export var speed_min = 150
export var speed_max = 250


func _ready():
	# pick a random animation
	var mob_types = $AnimatedSprite.frames.get_animation_names()
	var anim_name = mob_types[randi() % mob_types.size()]
	
	$AnimatedSprite.animation = anim_name
	$AnimationPlayer.play(anim_name)
	
	print_debug(anim_name)
	
	# choose the custom colider
	if anim_name == "walk":
		$CollisionShapeWalk.set_deferred("disabled", false)
	elif anim_name == "fly":
		$CollisionShapeFly.set_deferred("disabled", false)
		$CollisionShapeFlyArm.set_deferred("disabled", false)
	elif anim_name == "swim":
		$CollisionShapeSwim.set_deferred("disabled", false)


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
